import React from "react"
import "./App.css"

export default function Row(props){
    let boxes = [];
    for (let i = 0; i < props.n; i++) {
        props.id%2 === 0 ? boxes.push(<div key={i} className={i%2 === 0 ? "grid white" : "grid black"}></div>) : boxes.push(<div key={i} className={i%2 === 0 ? "grid black" : "grid white"}></div>)
        
    }
    return (
        <div className="row">
            {boxes}
        </div>
    )
}