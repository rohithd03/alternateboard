import React, { useState } from "react"
import "./App.css"
import Row from "./Row"

function App() {
    const [number, setNumber] = useState(5)
    let rows = []
    for (let i = 0; i < number; i++) {
        rows.push(<Row id={i} n={number} />)
    }

    return (
        <div className="App">
            <label>Enter n: </label>
            <input type="number" value={number} min={0} onChange={(e) => {setNumber(e.target.value)}} />
            <div className="rows">
                {rows}
            </div>
        </div>
    )
}

export default App;